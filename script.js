// alert, confirm, prompt

// Типы данных
// Переменные

// console.log('привет')

// Тип данных - категория информации(продукта):
// 1. Примитивные и Ссылочные
// Number(цифры): 1, 20, 20.5, 0.5
// String(строки): 'text', 'asd', 'Alex'
// Boolean(логический): true, false
// Undefined - не определено

// console.log(5+10)
// console.log(5-10)
// console.log(5*10)
// console.log(5/10)

// console.log(20/3)
// console.log(20%3)

// % - остаток от деления

// typeof - проверяет тип данных значения

// console.log(typeof 123);

// конкатенация - соединение строк

// console.log('Rustem' + 'Birnazarov')

// преобразование типов - цифра+строка=строка
// когда между значениями мы пишем всё кроме + и есть строка,
// вычисляются как числа. Если ставится+то происходит конкатенация

// console.log('50'+5000);
// console.log('50'*5);

// console.log('50$'-20);
// console.log('asd'-20);

// isNaN - проверка на NaN, если true, то значение равно NaN,
// если false - то значение обычное число

// console.log(isNaN(123));

// когда javascript не может преобразовать строку в цифру выходит NaN

// isFinite

// console.log(1/0);

// console.log('Сумма двух чисел', 20+5);

// Переменная - ячейка памяти
// var, let, const

// var name; // создается, инициализируется переменная

// name = 'Ivan' // присвоение

// var name = 'Andre'; // создание и присвоение

// console.log(name);

// 1. var - создается глобально
// 2. перезапись уже существующей переменной

// var config = 'PASDASOIHO*@(C780rtn238';

// var config = 'fast';

// console.log(test);

// var test = 'test';

// console.log(test);

// let, const

// let name = 'test'

// let name = 'asd'

// console.log(name);

// const name = 'test'
// name = 'asd'

// console.log(name);

// const - нельзя перезаписывать

// const discount = '10%';
// const price = 5000;

// const diccountPrice = price*discount;

// Int(integer) - целое
// Float - вещественные числа(с точкой), десятичные, дробные

// parseInt/parseFloat - достаёт из строки числа

// console.log(parseFloat('20.5px'));

// const discount = '10%';
// const price = 5000;

// snake_case - каждое новое слово разделяется нижним подчёркиванием
//camelCase - каждое новое слово пишется с большой буквы

// const discountPrice = (price*parseInt(discount))/100;

// const finallyPrice = price - discountPrice;

// console.log('Цена с учётом скидки:', finallyPrice);

// console.log(discountPrice);

// const number = 79;

// const ten = Math.floor(number/10);
// const ed = number%10;

// const res = ten+ed*10;

// console.log(ed, ten, res);

// Math - точка открывает то, что в консоле
// Math.round() - округляет по математическим правилам до целого
// Math.ceil() - округляет в большую сторону до целого
// Math.floor() - округляет в меньшую сторону до целого

// console.log(Math.floor(5.1));

// нумерация задач

// 1.

// const number = 2345;

// const hundred = Math.floor(number/100)

// const ten = number%100;

// console.log(ten);


// 2.

// const number = 1234;

// const ten = Math.floor(number%100);

// const thousand = Math.floor(ten/10);

// console.log(ten, thousand);

// 3.

const number = 123;

const ed = Math.floor(number/100);

const ten = Math.floor(number%10)

const min = ten - ed;

const res = ed+ten+min;

console.log(ed, ten, min, res);